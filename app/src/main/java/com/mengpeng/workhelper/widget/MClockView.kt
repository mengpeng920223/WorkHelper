package com.mengpeng.workhelper.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View

class MClockView : View {

    private var mContext: Context? = null

    constructor(context: Context?) : this(context, null) {}
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0) {}
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr) {

        this.mContext = context

        initView(attrs, defStyleAttr)
    }

    private fun initView(attrs: AttributeSet?, defStyleAttr: Int) {

    }
}