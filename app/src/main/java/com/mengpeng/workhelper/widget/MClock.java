package com.mengpeng.workhelper.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;

public class MClock extends View {
    private Context mContext;
    @ColorInt
    private int secondTextColor = Color.parseColor("#999999");
    @ColorInt
    private int minuteTextColor = Color.parseColor("#666666");
    @ColorInt
    private int hoursTextColor = Color.parseColor("#333333");
    private Paint mPaint;


    public MClock(Context context) {
        this(context, null);
    }

    public MClock(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MClock(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView(attrs, defStyleAttr);
    }

    private void initView(AttributeSet attrs, int defStyleAttr) {
        initPaint();

    }

    private void initPaint() {
        mPaint = new Paint();
        //抗锯齿
        mPaint.setAntiAlias(true);
        //防止抖动
        mPaint.setDither(true);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        if (widthMode == MeasureSpec.AT_MOST) {
            try {
                throw new Throwable("宽不能为wrap_content");
            } catch (Throwable throwable) {
                Log.d("MClock", "Throwable(\"宽不能为wrap_content\")");
                throwable.printStackTrace();
            }
        } else {
            int width = MeasureSpec.getSize(widthMeasureSpec);
            //视图最小要是个正方形，高要大于等于宽
            int height = Math.max(width, MeasureSpec.getSize(heightMeasureSpec));
            setMeasuredDimension(width, height);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //1.绘制背景
        drawBackground(canvas);
        //绘制圆心点
        mPaint.setColor(Color.parseColor("#ff00ff"));
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, 10, mPaint);
        //2.绘制秒
        drawSecond(canvas);
    }

    /**
     * 1.绘制背景
     */
    private void drawBackground(Canvas canvas) {
        //保存一下画笔
        canvas.save();
        //把画笔的原点初始化到坐标为（x，y）
        canvas.translate(0, 0);
        Rect rect = new Rect();
        rect.left = 0;
        rect.top = 0;
        rect.right = getWidth();
        rect.bottom = getHeight();
        mPaint.setColor(Color.parseColor("#33ff0000"));
        canvas.drawCircle((float) (getWidth() / 2), (float) (getHeight() / 2), (float) (getWidth() / 2 - 10), mPaint);
    }

    /**
     * 2.绘制秒
     */
    private void drawSecond(Canvas canvas) {
        //保存一下画笔
        canvas.save();
        //把画笔的原点初始化到坐标为（x，y）
        canvas.translate(0, 0);

        for (int i = 0; i < 60; i++) {
            Rect rect = new Rect();
            rect.top = (getHeight() - getWidth()) / 2 + 20;
            if (i % 5 == 0) {
                rect.left = (getWidth() / 2) - 3;
                rect.right = (getWidth() / 2) + 3;
                rect.bottom = (getHeight() - getWidth()) / 2 + 60;
                mPaint.setColor(Color.parseColor("#333333"));
                mPaint.setTextSize(36);
            } else {
                rect.left = (getWidth() / 2) - 2;
                rect.right = (getWidth() / 2) + 2;
                rect.bottom = (getHeight() - getWidth()) / 2 + 40;
                mPaint.setColor(Color.parseColor("#666666"));
                mPaint.setTextSize(20);
            }
            canvas.drawRect(rect, mPaint);
            mPaint.setTextAlign(Paint.Align.CENTER);
            canvas.drawText("" + i, (float) (getWidth() / 2), (float) ((getHeight() - getWidth()) / 2) + 95, mPaint);
            canvas.rotate(6, (float) (getWidth() / 2), (float) (getHeight() / 2));
            canvas.save();
        }

    }
}
