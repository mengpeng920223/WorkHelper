package com.mengpeng.workhelper.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import org.jetbrains.annotations.NotNull;

/**
 * logger工具类，使用之前先添加依赖 implementation 'com.orhanobut:logger:2.2.0'
 * 使用方法：
 * 1.在application中初始化： LoggerHelper.initLogger(this);
 * 2.其他的使用跟原本一样，封装的目的就在于当app是debug模式时可以输出日志，但打包之后就禁止输出日志。
 */
public class LoggerHelper {
    private static boolean isDebugMode = true;

    public static void initLogger(Context context) {
        Logger.addLogAdapter(new AndroidLogAdapter());
        try {
            ApplicationInfo applicationInfo = context.getApplicationInfo();
            isDebugMode = (applicationInfo.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }

    public static class tag {
        private String tags;

        public tag(@NotNull String tag) {
            this.tags = tag;
        }

        public void d(@Nullable Object object) {
            if (isDebugMode) {
                if (!TextUtils.isEmpty(this.tags)) {
                    Logger.t(this.tags).d(object);
                } else {
                    Logger.d(object);
                }
            }
        }

        public void log(int priority, @Nullable String tag, @Nullable String message, @Nullable Throwable throwable) {
            if (isDebugMode) {
                if (!TextUtils.isEmpty(this.tags)) {
                    Logger.t(this.tags).log(priority, tag, message, throwable);
                } else {
                    Logger.log(priority, tag, message, throwable);
                }
            }
        }

        public void d(@NonNull String message, @Nullable Object... args) {
            if (isDebugMode) {
                if (!TextUtils.isEmpty(this.tags)) {
                    Logger.t(this.tags).d(message, args);
                } else {
                    Logger.d(message, args);
                }
            }
        }

        public void e(@NonNull String message, @Nullable Object... args) {
            if (isDebugMode) {
                if (!TextUtils.isEmpty(this.tags)) {
                    Logger.t(this.tags).e(null, message, args);
                } else {
                    Logger.e(null, message, args);
                }
            }
        }

        public void e(@Nullable Throwable throwable, @NonNull String message, @Nullable Object... args) {
            if (isDebugMode) {
                if (!TextUtils.isEmpty(this.tags)) {
                    Logger.t(this.tags).e(throwable, message, args);
                } else {
                    Logger.e(throwable, message, args);
                }
            }
        }

        public void i(@NonNull String message, @Nullable Object... args) {
            if (isDebugMode) {
                if (!TextUtils.isEmpty(this.tags)) {
                    Logger.t(this.tags).i(message, args);
                } else {
                    Logger.i(message, args);
                }
            }
        }

        public void v(@NonNull String message, @Nullable Object... args) {
            if (isDebugMode) {
                if (!TextUtils.isEmpty(this.tags)) {
                    Logger.t(this.tags).v(message, args);
                } else {
                    Logger.v(message, args);
                }
            }
        }

        public void w(@NonNull String message, @Nullable Object... args) {
            if (isDebugMode) {
                if (!TextUtils.isEmpty(this.tags)) {
                    Logger.t(this.tags).w(message, args);
                } else {
                    Logger.w(message, args);
                }
            }
        }

        public void wtf(@NonNull String message, @Nullable Object... args) {
            if (isDebugMode) {
                if (!TextUtils.isEmpty(this.tags)) {
                    Logger.t(this.tags).wtf(message, args);
                } else {
                    Logger.wtf(message, args);
                }
            }
        }

        public void json(@Nullable String json) {
            if (isDebugMode) {
                if (!TextUtils.isEmpty(this.tags)) {
                    Logger.t(this.tags).json(json);
                } else {
                    Logger.json(json);
                }
            }
        }

        public void xml(@Nullable String xml) {
            if (isDebugMode) {
                if (!TextUtils.isEmpty(this.tags)) {
                    Logger.t(this.tags).xml(xml);
                } else {
                    Logger.xml(xml);
                }
            }
        }
    }

    public static void d(@Nullable Object object) {
        if (isDebugMode) {
            Logger.d(object);
        }
    }

    public static void log(int priority, @Nullable String tag, @Nullable String message, @Nullable Throwable throwable) {
        if (isDebugMode) {
            Logger.log(priority, tag, message, throwable);
        }
    }

    public static void d(@NonNull String message, @Nullable Object... args) {
        if (isDebugMode) {
            Logger.d(message, args);
        }
    }

    public static void e(@NonNull String message, @Nullable Object... args) {
        if (isDebugMode) {
            Logger.e(null, message, args);
        }
    }

    public static void e(@Nullable Throwable throwable, @NonNull String message, @Nullable Object... args) {
        if (isDebugMode) {
            Logger.e(throwable, message, args);
        }
    }

    public static void i(@NonNull String message, @Nullable Object... args) {
        if (isDebugMode) {
            Logger.i(message, args);
        }
    }

    public static void v(@NonNull String message, @Nullable Object... args) {
        if (isDebugMode) {
            Logger.v(message, args);
        }
    }

    public static void w(@NonNull String message, @Nullable Object... args) {
        if (isDebugMode) {
            Logger.w(message, args);
        }
    }

    public static void wtf(@NonNull String message, @Nullable Object... args) {
        if (isDebugMode) {
            Logger.wtf(message, args);
        }
    }

    public static void json(@Nullable String json) {
        if (isDebugMode) {
            Logger.json(json);
        }
    }

    public static void xml(@Nullable String xml) {
        if (isDebugMode) {
            Logger.xml(xml);
        }
    }
}
