package com.mengpeng.workhelper.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mengpeng.workhelper.event.MEventBus
import com.mengpeng.workhelper.event.MEventMessage
import com.mengpeng.workhelper.utils.StatusBarHelper
import com.mengpeng.workhelper.utils.StatusBarUtils
import com.umeng.message.PushAgent
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MEventBus.getInstance().register(this)
        PushAgent.getInstance(this).onAppStart()

        //去掉状态栏
        //StatusBarUtils.fullScreen(this@BaseActivity)
        //设置状态栏主题模式(字体颜色[白天模式-字体黑色，夜间模式-字体白色]）
        //StatusBarUtils.setStatusBarLightMode(this@BaseActivity)

        StatusBarHelper.setTranslucentStatusBars(this@BaseActivity)

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    open fun onMessageEvent(event: MEventMessage?) {

    }

    override fun onDestroy() {
        super.onDestroy()
        MEventBus.getInstance().unregister(this)
    }
}
