package com.mengpeng.workhelper;

import android.app.Application;
import android.app.Notification;
import android.content.Context;

import com.mengpeng.workhelper.event.MEventMessage;
import com.mengpeng.workhelper.utils.LoggerHelper;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.message.IUmengRegisterCallback;
import com.umeng.message.PushAgent;
import com.umeng.message.UmengMessageHandler;
import com.umeng.message.entity.UMessage;

import org.greenrobot.eventbus.EventBus;

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        initLogger();

        initUMengPushSdk();
    }

    private void initLogger() {
        LoggerHelper.initLogger(this);
    }

    private void initUMengPushSdk() {
        UMConfigure.init(this,
                "5e845ed50cafb2ade40004cb",
                "Umeng", UMConfigure.DEVICE_TYPE_PHONE,
                "a1e2d04bd0db9a0012bbcfaf52ef5c1d");
        PushAgent pushAgent = PushAgent.getInstance(this);
        pushAgent.register(new IUmengRegisterCallback() {
            @Override
            public void onSuccess(String deviceToken) {
                LoggerHelper.d("UMeng push init success ! deviceToken:" + deviceToken);
            }

            @Override
            public void onFailure(String s, String s1) {
                LoggerHelper.d("UMeng push init failure ! S:" + s + ",S1:" + s1);
            }
        });

        UmengMessageHandler messageHandler = new UmengMessageHandler() {
            @Override
            public Notification getNotification(Context context, UMessage msg) {
                String text = msg.text;
                LoggerHelper.d("UMeng text:" + text + ";title:" + msg.title);
                EventBus.getDefault().post(new MEventMessage(0, "0", text));
                return super.getNotification(context, msg);
            }
        };
        pushAgent.setMessageHandler(messageHandler);
    }
}
