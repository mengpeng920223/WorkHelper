package com.mengpeng.workhelper.event;

/**
 * 模板传递信息bean类
 * 说明：
 * 1.eventbus能传递的信息远不止这些，这里提供的只是简单的值，用的时候可以根据自己需要来自定义传值信息
 * 2.关于eventAction指的是传递action类型，例如网络请求中请求成功时做成功时的操作，失败的时候做失败操作，只用来
 * 分类，可以省略不用，但不推荐
 * 3.关于eventType指的是传递信息中的指定字段，用此字段来判断消息发送的身份证明
 * 4.关于eventAction和eventType的用法举例：假如网络请求时回调用的eventType是“load_message_online”,那么"load_message_online"
 * 指的就是这个网络请求，成功时eventAction == MEventCommon.MEventAction.MEventAction_OK ，失败的时候eventAction == MEventCommon.MEventAction.MEventAction_Error
 * 此时就不需要重新定义一个新的eventType了
 */
public class MEventMessage {

    private int eventAction;
    private String eventType;
    private int evtntIntMsg;
    private String eventStringMsg;
    private boolean eventBooleanMsg;
    private float eventFloatMsg;
    private double eventDoubleMsg;

    public MEventMessage(int eventAction, String eventType, int evtntIntMsg) {
        this.eventAction = eventAction;
        this.eventType = eventType;
        this.evtntIntMsg = evtntIntMsg;
    }

    public MEventMessage(int eventAction, String eventType, String eventStringMsg) {
        this.eventAction = eventAction;
        this.eventType = eventType;
        this.eventStringMsg = eventStringMsg;
    }

    public MEventMessage(int eventAction, String eventType, boolean eventBooleanMsg) {
        this.eventAction = eventAction;
        this.eventType = eventType;
        this.eventBooleanMsg = eventBooleanMsg;
    }

    public MEventMessage(int eventAction, String eventType, float eventFloatMsg) {
        this.eventAction = eventAction;
        this.eventType = eventType;
        this.eventFloatMsg = eventFloatMsg;
    }

    public MEventMessage(int eventAction, String eventType, double eventDoubleMsg) {
        this.eventAction = eventAction;
        this.eventType = eventType;
        this.eventDoubleMsg = eventDoubleMsg;
    }

    public int getEventAction() {
        return eventAction;
    }

    public String getEventType() {
        return eventType;
    }

    public int getEvtntIntMsg() {
        return evtntIntMsg;
    }

    public String getEventStringMsg() {
        return eventStringMsg;
    }

    public boolean isEventBooleanMsg() {
        return eventBooleanMsg;
    }

    public float getEventFloatMsg() {
        return eventFloatMsg;
    }

    public double getEventDoubleMsg() {
        return eventDoubleMsg;
    }


}
