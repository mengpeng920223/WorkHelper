package com.mengpeng.workhelper.event;

import org.greenrobot.eventbus.EventBus;

/**
 * EventBus封装工具类  使用之前先添加依赖 implementation 'org.greenrobot:eventbus:3.2.0'
 * 用法：
 * 1.在onstart()方法中调用 MEventBus.getInstance().register(Object subscriber); 来注册
 * 2.在onstop()方法中调用 MEventBus.getInstance().unregister(Object subscriber); 来解除注册
 * 3.发送信息主要就是构造MEventMessage，然后再调用MEventBus.getInstance().post(MEventMessage mEventMessage)
 * 4.其他的都是常用写法，简化步骤，也可以根据这个思想来做适合自己的定制化用法
 */
public class MEventBus {
    private static MEventBus mEventBus;

    public static MEventBus getInstance() {
        if (mEventBus == null) {
            mEventBus = new MEventBus();
        }
        return mEventBus;
    }

    /**
     * 注册eventbus
     */
    public void register(Object subscriber) {
        if (!isRegistered(subscriber)) {
            getEventBus().register(subscriber);
        }
    }

    /**
     * 解除注册eventbus
     */
    public void unregister(Object subscriber) {
        if (isRegistered(subscriber)) {
            getEventBus().unregister(subscriber);
        }
    }

    /**
     * eventbus是否注册了指定的subscriber
     */
    private boolean isRegistered(Object subscriber) {
        return getEventBus().isRegistered(subscriber);
    }

    /**
     * 获取EventBus实例
     */
    private EventBus getEventBus() {
        return EventBus.getDefault();
    }

    /**
     * 模板MEventMessage
     *
     * @param mEventMessage 要传递的信息
     */
    public void post(MEventMessage mEventMessage) {
        postEvent(mEventMessage);
    }

    /**
     * 统一传递的信息
     *
     * @param event 要传递的信息
     */
    public void postEvent(Object event) {
        getEventBus().post(event);
    }

    public void postIntOK(String mEventType, int msg) {
        post(new MEventMessage(MEventCommon.MEventAction.MEventAction_OK, mEventType, msg));
    }

    public void postStringOK(String mEventType, String msg) {
        post(new MEventMessage(MEventCommon.MEventAction.MEventAction_OK, mEventType, msg));
    }

    public void postFloatOK(String mEventType, float msg) {
        post(new MEventMessage(MEventCommon.MEventAction.MEventAction_OK, mEventType, msg));
    }

    public void postBooleanOK(String mEventType, boolean msg) {
        post(new MEventMessage(MEventCommon.MEventAction.MEventAction_OK, mEventType, msg));
    }

    public void postDoubleOK(String mEventType, double msg) {
        post(new MEventMessage(MEventCommon.MEventAction.MEventAction_OK, mEventType, msg));
    }

    public void postIntError(String mEventType, int msg) {
        post(new MEventMessage(MEventCommon.MEventAction.MEventAction_Error, mEventType, msg));
    }

    public void postStringError(String mEventType, String msg) {
        post(new MEventMessage(MEventCommon.MEventAction.MEventAction_Error, mEventType, msg));
    }

    public void postFloatError(String mEventType, float msg) {
        post(new MEventMessage(MEventCommon.MEventAction.MEventAction_Error, mEventType, msg));
    }

    public void postBooleanError(String mEventType, boolean msg) {
        post(new MEventMessage(MEventCommon.MEventAction.MEventAction_Error, mEventType, msg));
    }

    public void postDoubleError(String mEventType, double msg) {
        post(new MEventMessage(MEventCommon.MEventAction.MEventAction_Error, mEventType, msg));
    }

    public void postIntOther(String mEventType, int msg) {
        post(new MEventMessage(MEventCommon.MEventAction.MEventAction_Other, mEventType, msg));
    }

    public void postStringOther(String mEventType, String msg) {
        post(new MEventMessage(MEventCommon.MEventAction.MEventAction_Other, mEventType, msg));
    }

    public void postFloatOther(String mEventType, float msg) {
        post(new MEventMessage(MEventCommon.MEventAction.MEventAction_Other, mEventType, msg));
    }

    public void postBooleanOther(String mEventType, boolean msg) {
        post(new MEventMessage(MEventCommon.MEventAction.MEventAction_Other, mEventType, msg));
    }

    public void postDoubleOther(String mEventType, double msg) {
        post(new MEventMessage(MEventCommon.MEventAction.MEventAction_Other, mEventType, msg));
    }

}
